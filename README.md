# FI printers via Samba

Script for installing faculty printers on Linux via Samba print server.


## Advantages

-   You can print on Wi-Fi using Samba. (With unauthenticated IPP printing you are allowed to only print via Ethernet cable.)
-   You can add a FI printer to your existing CUPS instance (e.g. if you already have your own local printer). This way, you don&rsquo;t have to switch configurations.
-   You don&rsquo;t need to have the same login on your computer as your faculty login.


## Disadvantages

-   Printers have to be installed one by one.
-   Password will be stored at `/etc/cups/printers.conf` in plain text format.
-   This is not the preferred way of printing on FI. (The preferred way is via Kerberos.)


## Pre-requirements

-   root access (or sudo)
-   running local [CUPS](https://www.cups.org/) print server
-   Samba client (`smbclient`)
-   Cron system and git for automatic updating


### Warning

If you are printing via unauthenticated IPP or Kerberized IPP, you will have to remove the client configuration file for CUPS, which is located at `/etc/cups/client.conf`.


## Usage

Usage: `./install.pl {--cron | [--usage] <printer-name>}` , where `<printer-name>` is a valid printer name. If supplied `--usage`, the script will only update the drivers. Passing `--cron` will only prompt user for cron installation.

For example, to install the printer `lj2a`, run this within the cloned repository:

```shell
sudo ./install.pl lj2a
```

Installation script will prompt you for your faculty credentials (the faculty username and the faculty password).

You will be also asked if you want to install cron script for automatic updates if not already installed.

You may be prompted for the faculty credentials once again on the first print job, depending on your distribution.

### Supported printers

Currently supported printers:

-   copy2a
-   copy3a
-   copy4a
-   copy4c
-   lj2a
-   lj3a
-   lj-a302
-   lj3b
-   lj4a
-   lj4c
-   lj4p
-   lj5c

You can find out more information about printing in general
at <https://www.fi.muni.cz/tech/unix/print.html>.


## Tested Distributions

| Distribution | Status  |
|--------------|---------|
| Ubuntu 20.04 | Working |
| ArchLinux    | Working |

## Feedback

If you have any question, please contact us at [unix@fi.muni.cz](mailto:unix@fi.muni.cz).

## Repository structure

| File                     | Description                                      |
|------------------------- |------------------------------------------------- |
| `install.pl`             | main install script                              |
| `cron.pl`                | cron script for automatic updates                |
| `filters`                | folder with filters for CUPS                     |
| `ppds`                   | folder with PPDs for CUPS                        |
