#!/usr/bin/perl -w

use 5.24.0;
use strict;
use warnings;

use File::Copy;

my $REPO = "/opt/fi-printers";

if (-d $REPO) {
    chdir $REPO;

    my $output = `git pull --ff-only`;
    if ($output eq "Already up to date.\n") {
        exit;
    }
} else {
    `git clone https://gitlab.fi.muni.cz/unix/fi-printers.git $REPO`;
    chdir $REPO;
}

# copy cron.pl into weekly cron
copy("$REPO/cron.pl", "/etc/cron.weekly/fi_printers_cron")
    or die "Could not update /etc/cron.weekly/fi_printers_cron";

my @files = glob('/etc/cups/ppd/*@fi.ppd');

for my $item (@files) {
    print "current $item\n";
    $item =~ s/\/etc\/cups\/ppd\///;
    $item =~ s/\@fi\.ppd//;
}

for my $printer (@files) {
    system("$REPO/install.pl --update $printer");
    if ($? != 0) {
        warn "Error while updating $printer";
    }
}
