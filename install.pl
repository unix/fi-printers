#!/usr/bin/perl

use 5.24.0;
use strict;
use warnings;

use Carp qw/croak/;
use Cwd qw(cwd);
use File::Basename;
use File::Copy;
use File::Path;
use FindBin '$RealBin';
use Getopt::Long qw(:config bundling);

#--------------------------------------------------------------------
#  DATA
#--------------------------------------------------------------------

my $printers = {
    'copy3a' => {
        'model'    => 'TA4505ci',
        'location' => 'Building A, 3th floor kitchen',
    },
    'lj5c' => {
        'model'    => 'hplj600',
        'location' => 'Building C, 5th floor',
    },
    'lj4c' => {
        'model'    => 'hplj600',
        'location' => 'Building C, 4th floor',
    },
    'lj4p' => {
        'model'    => 'hpljp4015x',
        'location' => 'Building B, 4th floor',
    },
    'lj4a' => {
        'model'    => 'hplj600',
        'location' => 'Building A, 4th floor kitchen',
    },
    'copy4a' => {
        'model'    => 'TA4505ci',
        'location' => 'Building A, 4th floor kitchen',
    },
    'copy2a' => {
        'model'    => 'TA4505ci',
        'location' => 'Building A, 2nd floor kitchen',
    },
    'lj3a' => {
        'model'    => 'hplj600',
        'location' => 'Building A, 3th floor kitchen',
    },
    'lj2a' => {
        'model'    => 'hplj600',
        'location' => 'Building A, 2nd floor kitchen',
    },
    'lj3b' => {
        'model'    => 'hpljp4515x',
        'location' => 'Building B, 3th floor kitchen',
    },
    'lj-a302' => {
        'model'    => 'hplj600',
        'location' => 'Building A, 3th floor A302 LaSArIS',
    },
    'copy4c' => {
        'model'    => 'TA4505ci',
        'location' => 'Building C, 4th floor',
    }
};

my $models = {
    'TA5007ci' => {
        'filters' => [
            '/usr/lib/cups/filter/kyofilter_pre_E',
            '/usr/lib/cups/filter/kyofilter_E',
        ],
    },
    'TA4505ci' => {
        'filters' => ['/usr/libexec/cups/filter/kyofilter_B',],
        'options' => ['Option19=One', 'Option21=True', 'Option17=DF730',],
    },
    'hplj600' => {
        'options' => ['HPOption_Duplexer=True',],
    }
};

#--------------------------------------------------------------------
#  Option processing
#--------------------------------------------------------------------

my $options = {};

GetOptions($options, qw(update cron help|h|?))
  or usage();

if ($options->{usage} || (@ARGV != 1 && !$options->{cron})) {
    usage(0);
}

check_env();

if ($options->{cron}) {
    check_cron();
    exit;
}

#--------------------------------------------------------------------
#  Helper functions
#--------------------------------------------------------------------

sub get_description {
    my ($printer_name) = @_;
    return "$printer_name (FI MUNI)";
}

sub prompt_username {
    print "Enter your faculty login: ";
    chomp(my $username = <STDIN>);
    return $username;
}

sub prompt_secret {
    my ($prompt) = @_;
    print "$prompt: ";

    local $SIG{INT} = sub {
        system("stty echo");
        exit 1;
    };

    my $value;
    system("stty -echo");
    chomp($value = <STDIN>);
    system("stty echo");

    print "\n";
    return $value;
}

sub prompt_password {
    my $password1 = prompt_secret("Enter your faculty password");
    my $password2 = prompt_secret("Repeat your faculty password");

    if ($password1 ne $password2) {
        die "[ERROR] Passwords do not match!";
    }
    return $password1;
}

sub encode_uri {
    my ($source) = @_;
    $source =~ s/([^^A-Za-z0-9])/ sprintf "%%%02x", ord $1 /eg;
    return $source
}

sub get_url {
    my ($printer_name) = @_;
    my $user           = encode_uri(prompt_username());
    say "Warning! Password will be stored in plain text in /etc/cups/printers.conf";
    say "This is a limitation of the CUPS/SMB method. To exit press Ctrl + c.";
    my $password       = encode_uri(prompt_password());
    $printer_name = encode_uri($printer_name);

    return "smb://NTFI\\${user}:${password}\@print.fi.muni.cz/$printer_name";
}

sub install_printer {
    my ($printer_name) = @_;
    my $url = get_url($printer_name);

    my $command = qq(lpadmin \\
    -p "${printer_name}\@fi" \\
    -D "${printer_name} (FI MUNI)" \\
    -L "$printers->{$printer_name}->{'location'}" \\
    -E \\
    -v "${url}" \\
    -P "ppds/${printer_name}.ppd" \\
    -o media-default=iso_a4_210x297mm \\
    -o sides-default=two-sided-long-edge \\
    -o auth-info-required=username,password);

    my $model = $printers->{$printer_name}->{'model'};
    for my $option (@{$models->{$model}->{'options'}}) {
        $command .= " -o $option ";
    }

    system($command);
    if ($? != 0) {
        die "[ERROR] Could not install the printer via lpadmin";
    }

    return;
}

sub update_ppd {
    my ($printer_name) = @_;

    my $command = qq(lpadmin \\
    -p "${printer_name}\@fi" \\
    -P "ppds/${printer_name}.ppd" );

    system($command);
    if ($? != 0) {
        die "[ERROR] Could not update the PPD via lpadmin";
    }

    return;
}

sub copy_filter {
    my ($path) = @_;

    my ($filter, $destination, $suffix) = fileparse($path);

    # my $destination = "/usr/lib/cups/filter/";

    if (!-d $destination) {
        my $dirs = eval { mkpath($destination) };
        die "[ERROR] Failed to create $destination $@\n" unless $dirs;
    }

    copy("filters/" . $filter, $destination)
      or die "[ERROR] Failed to copy $filter: $!\n";
    say "[OK] Copied $filter.";

    chmod 0755, $destination . $filter;

    return;
}

sub print_printers {
    say "Available printers:";
    printf "%-14s | %-12s| %s\n", "Printer name", "Model", "Location";
    say "-" x 70;
    for (sort keys %$printers) {
        printf "%-14s | %-12s| %s\n", $_, $printers->{$_}->{'model'},
          $printers->{$_}->{'location'};
    }
    return;
}

sub usage {
    my ($exit_status) = @_;
    $exit_status //= 1;

    print <<END;
Script for installing faculty printers on Linux via Samba print server.
usage: $0 {--cron | [--update] <printer-name>}

Requires local CUPS server running and Samba client installed.
Must be run as root.
Option --update only updates installed printer.
Option --cron only prompts for cron installation.

END

    print_printers();

    exit $exit_status;
}

sub check_python {
    my $regex  = qr/^Python 2\.7*/;
    my $output = qx/python -V 2>&1/;

    if ($? == 0 && $output =~ $regex) {
        say "[OK] Python 2.7 is present.";
        return "python";
    }

    say "[ERROR] Python 2.7 is not present";

    my $s_output = qx/python2 -V 2>&1/;
    if ($? == 0 && $s_output =~ $regex) {
        say "[OK] Python 2.7 is present as python2";
        return "python2";
    }

    die "[ERROR] Python 2.7 Not Found and is needed for TA5007ci\n";
}

sub python_package {
    my ($package_name, $python) = @_;

    say "[INFO] Installing $package_name...";
    my $status = chdir "$package_name";
    if ($status == 0) {
        die "[ERROR] folder $package_name is not present!";
    }
    my $output = system("${python} setup.py install");
    if ($? != 0) {
        die "[ERROR] $package_name exited with different exit code than 0!";
    }
    $status = chdir '..';
    if ($status == 0) {
        die "[ERROR] Could not return to the working directory!";
    }
    return;
}

sub pre_TA5007ci {
    my $python = check_python();
    python_package("PyPDF2-1.26.0", $python);
    python_package("reportlab",     $python);
    return;
}

sub check_cron {
    if (! -e "/etc/cron.weekly/fi_printers_cron") {
        print "Do you want to enable automatic updates via cron? [Y/n] ";

        chomp(my $answer = <STDIN>);
        if ($answer eq 'Y' || $answer eq 'y' || $answer eq '') {
            say "[INFO] Installing cron...";

            copy("$RealBin/cron.pl", "/etc/cron.weekly/fi_printers_cron")
                or die "[ERROR] Could not copy cron script\n";
            chmod 0700, "/etc/cron.weekly/fi_printers_cron";
            say "[INFO] Cron installed.";
        }
    } else {
        say "[INFO] Cron is already installed.";
    }
}

sub check_env {
    # Check for smbclient
    `which smbclient 2>&1`;
    if ($? != 0) {
        die "[ERROR] No smbclient installed.";
    }

    # Check if we are root
    die "[ERROR] Must be run with root privileges\n" if $> != 0;
}
#--------------------------------------------------------------------
#  Start of the script
#--------------------------------------------------------------------

my ($printer_name) = @ARGV;

if (not(exists($printers->{$printer_name}))) {
    say "[ERROR] Printer $printer_name is not supported.";
    print_printers();
    exit 1;
}


my $model = $printers->{$printer_name}->{'model'};
say "[INFO] Selected model is $model";

#TA5007ci requires python2 reportlab and PyPDF2
if ($model eq "TA5007ci") {
    pre_TA5007ci;
}

for my $filter (@{($models->{$model} // {})->{filters} // []}) {
    copy_filter($filter);
}
if ($options->{update}) {
    update_ppd("$printer_name");
    say "[INFO] Update of $printer_name is completed.";
} else {
    install_printer("$printer_name");
    say "[INFO] Installation of $printer_name is completed.";
    check_cron();
}
